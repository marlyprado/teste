@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Sorteios</div>
                <div class="panel-body">
                    <p>Crie sorteios de amigo secreto entre participantes de um grupo.</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/raffle/create') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                            <label for="group_id" class="col-md-4 control-label">Grupo</label>

                            <div class="col-xs-6">
                                <select id="group_id" name="group_id" class="form-control">
                                    <option disabled selected>Selecione um Grupo</option>
                                    <option disabled>------------</option>
                                    @foreach($groups as $id => $group)
                                            <option value="{{ $id }}">{{ $group }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('group_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('group_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Sorteio</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Descrição</label>

                            <div class="col-md-6">
                                <input id="description" type="textarea" class="form-control" name="description" value="{{ old('description') }}">

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Sortear
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if (count($raffles) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Gerenciar seus grupos
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped raffle-table table-condensed">

                            <!-- Table Headings -->
                            <thead>
                                <th>Grupo</th>
                                <th>Sorteio</th>
                                <th>Descrição</th>
                            </thead>

                            <!-- Table Body -->
                            <tbody>
                                @foreach ($raffles as $raffle)
                                    <tr>
                                        <td class="table-text col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <div>{{ $raffle->group_name }}</div>
                                        </td>

                                        <td class="table-text col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <div>{{ $raffle->name }}</div>
                                        </td>

                                        <td class="table-text col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <div>{{ $raffle->description }}</div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Bem vindo!</div>

                <div class="panel-body">
                    <p>Quer fazer um sorteio de amigo secreto e não consegue reunir todo mundo?<br>
                    O Amigo X sortea para você e envia no e-mail de cada um.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grupos</div>
                <div class="panel-body">
                    <p>Crie grupos de amigos para realizar sorteios de amigo secreto entre eles.</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/group/create') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Criar grupo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if (count($groups) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Gerenciar seus grupos
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped group-table table-condensed">

                            <!-- Table Headings -->
                            <thead>
                                <th>Grupo</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </thead>

                            <!-- Table Body -->
                            <tbody>
                                @foreach ($groups as $group)
                                    <tr>
                                        <td class="table-text col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                            <div>{{ $group->name }}</div>
                                        </td>
                                        <td class="col-xs-2 col-sm-2 col-md-2 col-lg-2 white-space: nowrap">
                                            <form action="group/users/{{ $group->id }}" method="POST">
                                                {{ csrf_field() }}
                                                <button class="btn"><span class="glyphicon glyphicon-plus"></span> Amigos</button>
                                            </form>
                                        </td>
                                        <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1 white-space: nowrap">
                                            <form action="group/destroy/{{ $group->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Você participa dos grupos</div>
                <div class="panel-body">
                    <!-- <p>Crie grupos de amigos para realizar sorteios de amigo secreto entre eles.</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

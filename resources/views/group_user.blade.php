@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grupo: {{$group_name}}</div>

                <div class="panel-body">
                    <p>Adicionar amigos</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/group/users/'.$group_id.'/create') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Adicionar amigo ao grupo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if (count($group_users) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Gerenciar seus amigos no grupo
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped group_user-table table-condensed">

                            <!-- Table Headings -->
                            <thead>
                                <th>Amigo</th>
                                <th>E-mail</th>
                                <th>&nbsp;</th>
                            </thead>

                            <!-- Table Body -->
                            <tbody>
                                @foreach ($group_users as $group_user)
                                    <tr>
                                        <td class="table-text col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                            <div>{{ $group_user->name }}</div>
                                        </td>
                                        <td class="table-text col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <div>{{ $group_user->email }}</div>
                                        </td>
                                        <td class="col-xs-1 col-sm-1 col-md-1 col-lg-1 white-space: nowrap">
                                            <!-- <form action="group_user/destroy/{{ $group_user->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                                            </form> -->
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

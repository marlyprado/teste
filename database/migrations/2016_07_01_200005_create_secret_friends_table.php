<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecretFriendsTable extends Migration
{
    /**
     * Run the migrations.
     * Tabela de amigo secreto dos sorteios.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secret_friends', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('raffle_id')->index()->unsigned();
            $table->foreign('raffle_id')->references('id')->on('raffles')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('from_user_id')->index()->unsigned();
            $table->foreign('from_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('to_user_id')->index()->unsigned();
            $table->foreign('to_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secret_friends');
    }
}

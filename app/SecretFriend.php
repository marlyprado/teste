<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecretFriend extends Model
{
    protected $fillable = [
        'id', 'raffle_id', 'from_user_id', 'to_user_id', 'created_at', 'updated_at'
    ];
	// relationship
    public function from_user()
    {
        return $this->belongsTo(User::class,'id','from_user_id');
    }
    public function to_user()
    {
        return $this->belongsTo(User::class,'id','to_user_id');
    }
    public function raffle()
    {
        return $this->belongsTo(Raffle::class);
    }
}

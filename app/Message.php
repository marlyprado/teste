<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	// relationship
    public function from_user()
    {
        return $this->belongsTo(User::class,'id','from_user_id');
    }
    public function to_user()
    {
        return $this->belongsTo(User::class,'id','to_user_id');
    }
}

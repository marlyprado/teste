<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
	protected $fillable = [
        'id', 'group_id', 'name', 'description', 'created_at', 'updated_at'
    ];
	// relationship
    public function secret_friends()
    {
        return $this->hasMany(SecretFriend::class);
    }
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}

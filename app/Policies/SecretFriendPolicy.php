<?php

namespace App\Policies;

use App\SecretFriend;
use Illuminate\Auth\Access\HandlesAuthorization;

class SecretFriendPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}

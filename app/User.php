<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // relationship
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
    public function group_user()
    {
        return $this->hasMany(GroupUser::class);
    }

    public function from_secret_friends()
    {
        return $this->hasMany(SecretFriend::class,'from_user_id');
    }

    public function to_secret_friends()
    {
        return $this->hasMany(SecretFriend::class,'to_user_id');
    }

    public function from_messages()
    {
        return $this->hasMany(Message::class,'from_user_id');
    }

    public function to_messages()
    {
        return $this->hasMany(Message::class,'to_user_id');
    }
}

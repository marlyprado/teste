<?php

namespace App\Http\Controllers;

use Log;
use App\Group;
use App\Raffle;
use App\User;
use App\SecretFriend;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\RaffleRepository;
use Mail;

class RaffleController extends Controller
{
    protected $raffles;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Raffle $raffles)
    {
        $this->middleware('auth');
        $this->raffles = $raffles;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user()->id;
        $groups = Group::where('user_id','=',$user_id)->lists('name','id');
        $raffles = Raffle::select('raffles.id', 'raffles.group_id', 'raffles.name', 'raffles.description', 'raffles.created_at', 'raffles.updated_at','groups.name as group_name')
            ->join('groups',function($query)use($user_id){
                $query->on('groups.id','=','raffles.group_id')
                    ->where('groups.user_id','=',$user_id);
            })
            ->get();

        $data = [
            'groups' => $groups,
            'raffles' => $raffles,
        ];
        return view('raffle',$data);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'group_id' => 'required',
            'name' => 'required|max:255',
        ]);
        $group_id = $request->group_id;
        $item = [
            'group_id' => $group_id,
            'name' => $request->name,
            'description' => $request->description,
        ];
        $raffle = Raffle::create($item);
        if ($raffle)
        {
            $this->sortation($raffle, $group_id, $request);
        }

        return redirect('/raffle');
    }

    protected function sortation($raffle, $group_id,$request)
    {
        $raffle_id = $raffle->id;
        $friends = User::join('group_user',function($query)use($group_id){
                $query->on('group_user.user_id','=','users.id')
                    ->where('group_user.group_id','=',$group_id);
            })
            ->orderBy('users.id')
            ->lists('users.id')->toArray();

        $friends_total = User::join('group_user',function($query)use($group_id){
                $query->on('group_user.user_id','=','users.id')
                    ->where('group_user.group_id','=',$group_id);
            })
            ->count();

        $secret_friends = $this->recursiva($friends, $friends_total);

        foreach ($secret_friends as $key => $item) {
            $data = [
                'raffle_id' => $raffle_id,
                'from_user_id' => $item['from'],
                'to_user_id' => $item['to'],
            ];
            $id = SecretFriend::create($data);
            $from_user = User::find($item['from']);
            $to_user = User::find($item['to']);
            $data = [
                'raffle'    => $raffle,
                'from_user' => $from_user,
                'to_user'   => $to_user,
            ];
            Mail::send('emails.secret_friend', $data, function ($m) use ($request, $from_user) {
                $m->from('admin@marly.esy.es', 'Amigo X');
                $m->to($from_user->email, $from_user->name)->subject('Seu amigo secreto!');
            });
        }
    }

    protected function recursiva($friends, $friends_total)
    {
        $friends_sort = $friends;
        $friends_suffle = $friends;
        shuffle($friends_suffle);
        $raffle = true;
        $secret_friends = [];
        for ($i=0; $i < $friends_total; $i++)
        {
            if ($friends_sort[$i] == $friends_suffle[$i])
            {
                $raffle = false;
                $i = $friends_total;
            }
            else
            {
                $secret_friends[] = [
                    'from' => $friends_sort[$i],
                    'to'   => $friends_suffle[$i],
                ];
            }
        }
        if (! $raffle)
        {
            $secret_friends = $this->recursiva($friends, $friends_total);
        }
        return $secret_friends;
    }
}

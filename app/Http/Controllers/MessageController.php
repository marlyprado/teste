<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Log;
use App\Message;
use App\Http\Controllers\Controller;
use App\Repositories\MessageRepository;

class MessageController extends Controller
{
    protected $messages;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Message $messages)
    {
        $this->middleware('auth');
        $this->messages = $messages;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('msg', [
            'messages' => $this->messages->get(),
        ]);
    }
}

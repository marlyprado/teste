<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\GroupUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GroupUserRepository;

class GroupUserController extends Controller
{
    protected $group_user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GroupUserRepository $group_user)
    {
        $this->middleware('auth');
        $this->group_user = $group_user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('group_user');
    }
}

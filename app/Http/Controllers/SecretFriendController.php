<?php

namespace App\Http\Controllers;

use Log;
use App\SecretFriend;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GroupRepository;

class SecretFriendController extends Controller
{
    protected $secret_friends;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SecretFriendRepository $secret_friends)
    {
        $this->middleware('auth');
        $this->secret_friends = $secret_friends;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('secret_friends');
    }
}

<?php

namespace App\Http\Controllers;

use Log;
use App\Group;
use App\GroupUser;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GroupRepository;
use Mail;

class GroupController extends Controller
{
    protected $groups;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GroupRepository $groups)
    {
        $this->middleware('auth');
        $this->groups = $groups;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('group', [
            'groups' => $this->groups->forUser($request->user()),
        ]);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        $item = [
            'user_id' => $request->user()->id,
            'name' => $request->name
        ];
        Group::create($item);

        return redirect('/group');
    }

    public function destroy(Request $request, Group $group)
    {
        // $this->authorize('destroy', $group);
        $group->delete();

        return redirect('/group');
    }

    public function users(Request $request, Group $group)
    {
        $group_users = GroupUser::getUserByGroup($group);

        $view = 'group_user';
        $data = [
            'group_id'    => $group->id,
            'group_name'  => $group->name,
            'group_users' => $group_users
        ];
        return view('group_user', $data);
    }

    public function users_create(Request $request, Group $group)
    {
        $this->validate($request, [
            'e-mail' => 'email',
            'name' => 'required|max:255',
        ]);

        $friend = User::where('email','=',$request->email)->first();

        if (! $friend)
        {
            $friend =  User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => '',
            ]);
        }

        $item = [
            'group_id' => $group->id,
            'user_id' => $friend->id,
            'invitation_accepted' => 0
        ];
        $id = GroupUser::create($item);
        if ($id)
        {
            $owner = User::where('id','=',$group->user_id)->pluck('name');
            $data = [
                'owner' => $owner,
                'name'  => $request->name,
            ];
            Mail::send('emails.invite', $data, function ($m) use ($request) {
                $m->from('admin@marly.esy.es', 'Amigo X');
                $m->to($request->email, $request->name)->subject('Seu lembrete!');
            });

        }

        return redirect('/group/users/'.$group->id);
    }
}

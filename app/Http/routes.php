<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/group', 'GroupController@index');
Route::post('/group/create', 'GroupController@create');
Route::delete('/group/destroy/{group}', 'GroupController@destroy');
Route::any('/group/users/{group}', 'GroupController@users');
Route::post('/group/users/{group}/create', 'GroupController@users_create');

Route::get('/raffle', 'RaffleController@index');
Route::post('/raffle/create', 'RaffleController@create');

Route::get('/msg', 'MessageController@index');
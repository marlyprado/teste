<?php

namespace App;

use Log;
use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    protected $table = 'group_user';
	protected $fillable = [
        'id', 'group_id', 'user_id', 'invitation_accepted', 'created_at', 'updated_at'
    ];
	// relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    // queries
    public static function getUserByGroup($group)
    {
        $items = GroupUser::select('group_user.id', 'group_user.group_id', 'group_user.user_id', 'group_user.invitation_accepted', 'group_user.created_at', 'group_user.updated_at','users.name','users.email')
            ->leftJoin('users','users.id','=','group_user.user_id')
            ->where('group_id','=',$group->id)
            ->get();
        return $items;
    }
}

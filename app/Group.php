<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'id', 'user_id', 'name', 'created_at', 'updated_at'
    ];
	// relationship
	public function group_users()
    {
        return $this->hasMany(GroupUser::class);
    }
    public function raffles()
    {
        return $this->hasMany(Raffle::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

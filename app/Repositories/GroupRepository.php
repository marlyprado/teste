<?php

namespace App\Repositories;

use App\User;
use App\Group;

class GroupRepository
{
    public function forUser(User $user)
    {
        return Group::where('user_id', $user->id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}
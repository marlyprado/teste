<?php

namespace App\Repositories;

use App\User;
use App\Group;
use App\GroupUser;

class GroupUserRepository
{
    public function forGroupUser(Group $group, User $user)
    {
        return GroupUser::where('group_id', $group->id)
        			->where('user_id', $user->id)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
}